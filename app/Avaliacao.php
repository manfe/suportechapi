<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Avaliacao extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'avaliacao_id';
    protected $table = 'avaliacoes';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nota', 'comentário'  
    ];

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function prestador()
    {
        return $this->hasOne('App\Prestador');
    }

    public function sevico_prestado()
    {
        return $this->hasOne('App\ServicePrestado');
    }
}
