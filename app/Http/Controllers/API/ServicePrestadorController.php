<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ServicePrestadorCollection;
use App\Http\Resources\ServicePrestadorResource;

use App\ServicePrestador;

class ServicePrestadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serviceIds = $request->input('serviceIds');
        $prestadorId = $request->input('prestadorId');

        $servicesOld = ServicePrestador::where('prestador_id',$prestadorId)->get();

        foreach($servicesOld as $service){
            $service->delete();
        }

        foreach ($serviceIds as $id) {
            $servicePrestador = new ServicePrestador();
            $servicePrestador->prestador_id = $prestadorId;
            $servicePrestador->servico_id = $id;
            if($servicePrestador->save() != true){
                return response()->json(false);
            }
        }

        return response()->json(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
