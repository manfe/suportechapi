<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;

use App\Post;
use App\User;
use App\Prestador;
use App\Service;
use App\InteressePrestador;
use App\ServicePrestador;
use App\ServicePrestado;

class PostController extends Controller
{
    public function index()
    {
        // return new PostCollection(Post::all());
    }

    public function store(Request $request)
    {
        $_post = $request->input('post');
        $user = User::where('email','LIKE',$request->input("username"))->first();
        if ($user == null){
            return response()->json("Algo deu errado");
        }

        $post = new Post();
        $post->user_id = $user->user_id;
        $post->servico_id = $_post['servicoId'];
        $post->descricao = $_post['descricao'];
        $post->dia_semana = $_post['diaSemana'];
        $post->hora_inicial = date("H:i", strtotime($_post['horaInitial']) );
        $post->hora_final = date("H:i", strtotime($_post['horaFinal']) );

        if ($post->save()){
            return response()->json(true);
        } else{
            return response()->json(false);
        }
    }

    public function interesse(Request $request)
    {
        $user = User::Where('email', $request->input("username"))->first();
        $prestador = Prestador::where('user_id','=',$user->user_id)->first();
        $postId = $request->input("postId");
        $price = $request->input("price");

        $interesse = InteressePrestador::Where('post_id',$postId)->where('prestador_id',$prestador->prestador_id)->first();

        $interesse = new InteressePrestador();
        $interesse->descricao = $request->input("message");
        $interesse->prestador_id = $prestador->prestador_id;
        $interesse->post_id = $postId;
        if($price != null){
            $interesse->preco = floatval($price);
        }
        

        if ($interesse->save()){
            return response()->json(true);
        }
        return response()->json(false);
        
    }

    public function desinteresse(Request $request)
    {
        $user = User::Where('email', $request->input("username"))->first();
        $prestador = Prestador::where('user_id','=',$user->user_id)->first();
        $postId = $request->input("postId");

        $interesse = InteressePrestador::Where('post_id',$postId)->where('prestador_id',$prestador->prestador_id)->first();

        if ($interesse->delete()){
            return response()->json(true);
        }
        return response()->json(false);
        
    }

    public function show($username)
    {
        $user = User::Where('email', $username)->first();
        if($user != null){
            $post = Post::Where('user_id',$user->user_id)->get();
            return response()->json($post);
        }
        else{
            return response()->json(false);
        }
    }

    public function getPosts($username){
        $user = User::Where('email', $username)->first();
        if($user != null){
            $posts = Post::Where('user_id','<>',$user->user_id)->get();
            $prestador = Prestador::where('user_id','=',$user->user_id)->first();
            
            foreach ($posts as $key => $post) {
                $servicePrestado = ServicePrestado::Where('post_id', $post->post_id)->first();
                if ($servicePrestado != null){
                    unset($posts[$key]);
                    continue;
                } 
                $post->service_name = Service::Where('servico_id',$post->servico_id)->first()->nome;
                $post->solicitante = User::find($post->user_id)->first()->name;
            }

            if ($prestador == null){
                return new PostCollection($posts);
            }

            foreach ($posts as $key =>  $post) {
                $servicePrestador = ServicePrestador::Where('servico_id',$post->servico_id)->where('prestador_id',$prestador->prestador_id)->first();
                if($servicePrestador == null){
                    unset($posts[$key]);
                } else{
                    $interesse = InteressePrestador::Where('post_id',$post->post_id)->where('prestador_id',$prestador->prestador_id)->first();
                    if ($interesse == null){
                        $post->interesse = false;    
                    } else{
                        $post->interesse = true;    
                    }
                }
            } 

            return new PostCollection($posts);
        }
        else{
            return response()->json(false);
        }
    }

    public function getMyPosts($username){
        $user = User::Where('email', $username)->first();
        if($user != null){
            $posts = Post::Where('user_id',$user->user_id)->get();

            foreach ($posts as $key => $post) {
                $post->service_name = Service::Where('servico_id',$post->servico_id)->first()->nome;

                $servicePrestado = ServicePrestado::Where('post_id', $post->post_id)->first();
                if ($servicePrestado != null){
                    $post->concluido = true;
                } else{
                    $post->interesses;

                    foreach ($post->interesses as $key => $interesse) {
                        $userId = Prestador::Where('prestador_id',$interesse->prestador_id)->first()->user_id;
                        $name = User::Where('user_id',$userId)->first()->name;
                        $interesse->name_prestador = $name;
                    }
                }
                
            }

            return new PostCollection($posts);
        }
        else{
            return response()->json(false);
        }
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        if(Post::destroy($id)){
            return response()->json(true);
        } else{
            return response()->json(false);
        }
    }
}
