<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'post_id';
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'descricao', 'dia_semana', 'hora_inicial', 'hora_final'  
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function servico()
    {
        return $this->hasOne('App\Service');
    }

    public function interesses(){
        return $this->hasMany('App\InteressePrestador','post_id');
    }
}
