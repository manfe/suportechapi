<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicosPrestadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicos_prestados', function (Blueprint $table) {
            $table->increments('servico_prestado_id');
            //keys
            $table->unsignedInteger('prestador_id');
            $table->foreign('prestador_id')->references('prestador_id')->on('prestadores');

            $table->unsignedInteger('post_id');
            $table->foreign('post_id')->references('post_id')->on('posts');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicos_prestados');
    }
}
