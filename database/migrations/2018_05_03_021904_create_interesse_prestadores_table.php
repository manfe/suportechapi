<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteressePrestadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interesse_prestadores', function (Blueprint $table) {
            $table->increments('interesse_prestador_id');
            $table->string('descricao', 250)->nullable($value = true);
            $table->float('preco', 8, 2)->nullable($value = true);
            //keys
            $table->unsignedInteger('prestador_id');
            $table->foreign('prestador_id')->references('prestador_id')->on('prestadores');

            $table->unsignedInteger('post_id');
            $table->foreign('post_id')->references('post_id')->on('posts');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interesse_prestadores');
    }
}
