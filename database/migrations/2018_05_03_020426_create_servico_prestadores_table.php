<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicoPrestadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servico_prestadores', function (Blueprint $table) {
            $table->increments('servico_prestador_id');
            
            //keys
            $table->unsignedInteger('prestador_id');
            $table->foreign('prestador_id')->references('prestador_id')->on('prestadores');

            $table->unsignedInteger('servico_id');
            $table->foreign('servico_id')->references('servico_id')->on('servicos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servico_prestadores');
    }
}
