<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServicesSeeder extends Seeder
{
    public function run()
    {
        Service::create([
            'nome' => 'Formatação de computadores',
            'descricao' => 'Descrição',
        ]);

        Service::create([
            'nome' => 'Instalação de programas',
            'descricao' => 'Descrição',
        ]);

        Service::create([
            'nome' => 'Conserto de antenas e parabólicas',
            'descricao' => 'Descrição',
        ]);

        Service::create([
            'nome' => 'Conserto e venda de televisões',
            'descricao' => 'Descrição',
        ]);
    }
}